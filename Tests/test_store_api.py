import pprint
from conftest import *
from datetime import datetime

# # Store


@allure.feature('Работа с сущностью Магазина (Store)')
@allure.story('Добавить новый заказ')
@pytest.mark.parametrize(
    "order",
    [
        {
          "id": 16215326598755,
          "petId": 16215326598755,
          "quantity": 0,
          "shipDate": datetime.now().isoformat(sep='T'),
          "status": "placed",
          "complete": "true"
        },
        {
            "id": "",
            "petId": 1,
            "quantity": 0,
            "shipDate": datetime.now().isoformat(sep='T'),
            "status": "shipped",
            "complete": "true"
        },
        {
            "id": 2,
            "petId": 45,
            "quantity": 0,
            "shipDate": datetime.now().isoformat(sep='T'),
            "status": "shipped",
            "complete": "true"
        }
    ]
)
def test_create_order(api, order):
    response = api.post("store/order", json=order)
    with allure.step('Запрос отправлен. Статус и код ответа:'):
        pprint.pprint(f'Reason: {response.reason}, Status Code: {response.status_code}')
    # print(response.text)


@allure.feature('Работа с сущностью Магазина (Store)')
@allure.story('Получить информацию о заказе по его id')
@pytest.mark.parametrize(
    "order",
    [
        {"id": 16215326598755},
        {"id": 1},
        {"id": 2},
        {"id": 3},
        {"id": 4},
    ]
)
def test_get_order(api, order):
    response = api.get("store/order/" + str(order["id"]))
    with allure.step('Запрос отправлен. Статус и код ответа:'):
        pprint.pprint(f'Reason: {response.reason}, Status Code: {response.status_code}')
    # print(response.text)


@allure.feature('Работа с сущностью Магазина (Store)')
@allure.story('Удалить заказ из магазина по его id')
@pytest.mark.parametrize(
    "order",
    [
        {"id": 16215326598755},
        {"id": 1},
        {"id": 2},
        {"id": 3},
    ]
)
def test_delete_order(api, order):
    response = api.delete("store/order/" + str(order["id"]))
    with allure.step('Запрос отправлен. Статус и код ответа:'):
        pprint.pprint(f'Reason: {response.reason}, Status Code: {response.status_code}')
    # print(response.text)


# @allure.feature('Работа с сущностью Магазина (Store)')
# @allure.story('Получить информацию о заказе по его id')
# @pytest.mark.parametrize(
#     "order",
#     [
#         {"id": 16215326598755},
#         {"id": 1},
#         {"id": 2},
#         {"id": 3},
#         {"id": 4},
#     ]
# )
# def test_get_order(api, order):
#     response = api.get("store/order/" + str(order["id"]))
#     with allure.step('Запрос отправлен. Статус и код ответа:'):
#         pprint.pprint(f'Reason: {response.reason}, Status Code: {response.status_code}')
#     print(response.text)