import pprint
from conftest import *


# # Users

@allure.feature('Работа с сущностью Пользователи (User)')
@allure.story('Добавить нового пользователя')
@pytest.mark.parametrize(
    "user",
    [
        {"id": 16215326598755, "username": "johny", "firstName": "Johny", "lastName": "Boy", "email": "johny@boy.com", "password": "string1", "phone": "string", "userStatus": 0},
    ]
)
def test_create_user(api, user):
    response = api.post("user", json=user)
    with allure.step('Запрос отправлен. Статус и код ответа:'):
        pprint.pprint(f'Reason: {response.reason}, Status Code: {response.status_code}')
    # print(response.text)


@allure.feature('Работа с сущностью Пользователи (User)')
@allure.story('Запросить информацию о пользователе по полю username')
@pytest.mark.parametrize(
    "user",
    [
        {"username": "johny"},
        {"username": "johnyBoy"},
    ]
)
def test_get_user(api, user):
    response = api.get("user/" + str(user["username"]))
    with allure.step('Запрос отправлен. Статус и код ответа:'):
        pprint.pprint(f'Reason: {response.reason}, Status Code: {response.status_code}')
        # assert response.status_code == 200, 'Пользователь не найден'
    # print(response.text)


@allure.feature('Работа с сущностью Пользователи (User)')
@allure.story('Обновление данных пользователя')
@pytest.mark.parametrize(
    "user",
    [
        {"id": 16215326598755, "username": "johny", "firstName": "Johny", "lastName": "Boy", "email": "johnyBoy@boy.com", "password": "StrongPass", "phone": "string", "userStatus": 0},
    ]
)
def test_update_user(api, user):
    response = api.put("user/" + str(user["username"]), json=user)
    with allure.step('Запрос отправлен. Статус и код ответа:'):
        pprint.pprint(f'Reason: {response.reason}, Status Code: {response.status_code}')
    # print(response.text)


@allure.feature('Работа с сущностью Пользователи (User)')
@allure.story('Попытка залогиниться в систему')
@pytest.mark.parametrize(
    "user",
    [
        {"username": "johny", "password": "wrongPass"},
        {"username": "johny", "password": "StrongPass"},
    ]
)
def test_login_user(api, user):
    response = api.get(f'user/login?username={str(user["username"])}&password={str(user["password"])}')
    with allure.step('Запрос отправлен. Статус и код ответа:'):
        pprint.pprint(f'Reason: {response.reason}, Status Code: {response.status_code}')
    # print(response.text)


@allure.feature('Работа с сущностью Пользователи (User)')
@allure.story('Удаляем пользователя из системы')
@pytest.mark.parametrize(
    "user",
    [
        {"username": "johny"},
    ]
)
def test_delete_user(api, user):
    response = api.delete(f'user/{str(user["username"])}')
    with allure.step('Запрос отправлен. Статус и код ответа:'):
        pprint.pprint(f'Reason: {response.reason}, Status Code: {response.status_code}')
    # print(response.text)
