import pprint
from conftest import *


# # Pets
@allure.feature('Работа с сущностью животных (Pets)')
@allure.story('Поиск животного по его номеру id')
@pytest.mark.parametrize(
    "pet",
    [
        {"id": 0},
        {"id": 1},
        {"id": 2},
        {"id": 65674567884850305},
    ]
)
def test_get_pet(api, pet):
    response = api.get("pet/" + str(pet["id"]))
    with allure.step('Запрос отправлен. Статус и код ответа:'):
        pprint.pprint(f'Reason: {response.reason}, Status Code: {response.status_code}')
    # print(response.text)


@allure.feature('Работа с сущностью животных (Pets)')
@allure.story('Запрос животных по статусу')
@pytest.mark.parametrize(
    "pet",
    [
        {"status": 'available'},
        {"status": 'sold'},
    ]
)
def test_get_pets_by_status(api, pet):
    response = api.get("pet/findByStatus?status=" + str(pet["status"]))
    with allure.step('Запрос отправлен. Статус и код ответа:'):
        pprint.pprint(f'Reason: {response.reason}, Status Code: {response.status_code}')
    # print(response.text)


@allure.feature('Работа с сущностью животных (Pets)')
@allure.story('Добавление животного')
@pytest.mark.parametrize(
    "pet",
    [
        {'id': 65674567884850305, 'name': 'Bobby', 'status': 'available'},
    ]
)
def test_create_pet(api, pet):
    response = api.post("pet", json=pet)
    with allure.step('Запрос отправлен. Статус и код ответа:'):
        pprint.pprint(f'Reason: {response.reason}, Status Code: {response.status_code}')
        pprint.pprint(response.reason)


@allure.feature('Работа с сущностью животных (Pets)')
@allure.story('Изменение данных о животном')
@pytest.mark.parametrize(
    "pet",
    [
        {'id': 65674567884850305, 'name': 'Johny', 'status': 'available'},

    ]
)
def test_update_pet(api, pet):
    response = api.put("pet", json=pet)
    with allure.step('Запрос отправлен. Статус и код ответа:'):
        pprint.pprint(f'Reason: {response.reason}, Status Code: {response.status_code}')
    # pprint.pprint(response.reason)
    # print(f'Text:  {response.text}')


@allure.feature('Работа с сущностью животных (Pets)')
@allure.story('Удаление животных по их id')
@pytest.mark.parametrize(
    "pet",
    [
        {"id": 65674567884850305},
        {"id": 1},
        {"id": 2},
        {"id": 3},
    ]
)
def test_delete_pet(api, pet):
    response = api.delete("pet/" + str(pet["id"]), headers={"api-key": "special-key"})
    with allure.step('Запрос отправлен. Статус и код ответа:'):
        pprint.pprint(f'Reason: {response.reason}, Status Code: {response.status_code}')
    # pprint.pprint(response.reason)
    # print(f'Text:  {response.text}')
