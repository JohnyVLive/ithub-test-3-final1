import allure
import requests
import pytest


@pytest.fixture()
def api():
    return Api(address="https://petstore.swagger.io/v2/")


class Api:
    def __init__(self, address):
        self.address = address
        self._headers = {'accept': 'application/json'}

    def post(self, path="/", json=None, headers=None):
        url = f"{self.address}{path}"

        if headers is None:
            loc_headers = self._headers
            loc_headers['Content-Type'] = 'application/json'
        else:
            loc_headers = self._headers
            loc_headers.update(headers)

        with allure.step(f"POST request to: {url}"):
            return requests.post(url=url, json=json, headers=loc_headers, timeout=30)

    def get(self, path="/", headers={}):
        url = f"{self.address}{path}"
        with allure.step(f"GET request to: {url}"):
            return requests.get(url=url, headers=headers, timeout=30)

    def put(self, path="/", json=None, headers={}):
        url = f"{self.address}{path}"
        headers['Content-Type'] = 'application/json'
        with allure.step(f"PUT request to: {url}"):
            return requests.put(url=url, json=json, headers=headers, timeout=30)

    def delete(self, path="/", headers=None):
        url = f"{self.address}{path}"
        if headers is None:
            loc_headers = self._headers
        else:
            loc_headers = self._headers
            loc_headers.update(headers)
        with allure.step(f"Delete request to: {url}"):
            return requests.delete(url=url, headers=loc_headers, timeout=30)

    def head(self, path="/", headers={}):
        url = f"{self.address}{path}"
        return requests.head(url=url, headers=headers, timeout=30)

    def patch(self, path="/", json=None, headers={}):
        url = f"{self.address}{path}"
        # headers['accept'] = 'application/json'
        headers['Content-Type'] = 'application/json'
        return requests.patch(url=url, json=json, headers=headers, timeout=30)
